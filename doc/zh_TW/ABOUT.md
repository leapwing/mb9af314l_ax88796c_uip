## MB9AF314L\_AX88796C\_uIP Firmware ##

簡易的Webserver測試程式

硬體平台：MB9AF314L + AX88796C  
軟體平台：uIP  

介面:  
1. SPI baudrate 4MHz MFS:CH7  
2. UART baudrate 115200 MFS:CH4  
3. LED x 3(GPIO)  
4. Joystick x 1(GPIO)  
5. potentiomenter x 1(AN01)	  
6. GPIO for AX88796C RESET & SPI_SS

使用：   
使用實驗版來連線電腦網頁顯示狀態  

Screenshot  
![demo_1](https://lh4.googleusercontent.com/-CCpQ02i9L5Y/UpWkku1h0bI/AAAAAAAAAFI/Q32WA6yvkuc/w700-h557-no/fm9baf314l_ax88796c_uip_demo1.jpg)  
▲ Demo1 Screen    
![demo_2](https://lh4.googleusercontent.com/-5i8VeRAL-sE/UpWxZcF4BMI/AAAAAAAAAHE/bWHt_CB3bl8/w697-h589-no/fm9baf314l_ax88796c_uip_demo2.jpg)  
▲ Demo2 Screen  

